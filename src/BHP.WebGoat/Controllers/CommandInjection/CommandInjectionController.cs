using Microsoft.AspNetCore.Mvc;
using BHP.WebGoat.Services;

namespace BHP.WebGoat.Controllers.Weather;

[ApiController]
[Route("api/command-injection")]
public class CommandInjectionController : ControllerBase
{
    private readonly Services.IExecution _execution;

    public CommandInjectionController(
        Services.IExecution execution
    )
    {
        _execution = execution;
    }

    [HttpGet("{cmd}/{args}")]
    public async Task<IActionResult> GetAsync(string cmd, string args)
    {
        await Task.CompletedTask;
        _execution.Execute(cmd, args);
        return this.Ok();
    }
}
