using Microsoft.AspNetCore.Mvc;

namespace BHP.WebGoat.Controllers.Weather;

[ApiController]
[Route("api/sql-injection")]
public class SqlInjectionController : ControllerBase
{
    private readonly Services.IRepository _repository;

    public SqlInjectionController(
        Services.IRepository repository
    )
    {
        _repository = repository;
    }

    [HttpGet("{name}")]
    public async Task<IActionResult> GetAsync(string name)
    {
        await Task.CompletedTask;
        var message = _repository.GetPersonId(name);
        return this.Ok(message);
    }
}
