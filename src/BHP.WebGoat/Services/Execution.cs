using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.Diagnostics;

namespace BHP.WebGoat.Services
{
    public class Execution : IExecution
    {

        public void Execute(string cmd, string args) {
            /*/
            Console.WriteLine($"Hello {name}");
            /*/
            using var process = Process.Start(
                new ProcessStartInfo
                {
                    FileName = cmd,
                    ArgumentList = { args }
                }
            );
            process?.WaitForExit();
            //*/
        } 
    }
}