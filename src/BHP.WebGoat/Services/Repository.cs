using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;

namespace BHP.WebGoat.Services
{
    public class Repository : IRepository
    {
        private string ConnectionString => Environment.GetEnvironmentVariable("CONNECTION_STRING")?.ToString() ?? "";

        public string GetHello(string name) => $"Hello {name}";

        public string GetPersonId(string name) {
            var sql = "SELECT * from dbo.People where name like '" + name + "%'";
            using (var connection = new SqlConnection(this.ConnectionString)) {
                connection.Open();
                using (var command = new SqlCommand(sql)) {
                    using (var reader = command.ExecuteReader()) {
                        return reader.Read()
                            ?  reader["Id"].ToString() ?? ""
                            : "";
                    }
                }
            }
        }
    }
}