using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace BHP.WebGoat.Services
{
    public interface IRepository
    {
        string GetHello(string name);

        string GetPersonId(string name);
    }
}