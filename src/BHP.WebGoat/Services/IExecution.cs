using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;

namespace BHP.WebGoat.Services
{
    public interface IExecution
    {
        void Execute(string cmd, string args);
    }
}